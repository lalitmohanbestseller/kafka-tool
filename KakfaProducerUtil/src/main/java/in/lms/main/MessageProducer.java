package in.lms.main;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class MessageProducer {
	private final KafkaProducer<Integer, String> producer;

	private boolean isAsync;
	
	public static final String CLIENT_ID = "SampleProducer";

	public boolean getIsAsync() {
		return isAsync;
	}

	public void setIsAsync(boolean isAsync) {
		this.isAsync = isAsync;
	}

	public MessageProducer(String serverUrl) {
		Properties properties = new Properties();
		properties.put("bootstrap.servers", serverUrl);
		properties.put("client.id", CLIENT_ID);
		properties.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		producer = new KafkaProducer<Integer, String>(properties);

	}

	public void sendMessage(Data data) {
		System.out.printf("\nSending message : %s to topic : %s", data.getMessage(),data.getTopicName());
		int messageNo = 0;
		long startTime = System.currentTimeMillis();
		if (isAsync) {
			producer.send(new ProducerRecord<Integer, String>(data.getTopicName(), data.getMessage()),
					new MessageCallBack(startTime, data.getMessage()));
		} else {
			try {
				producer.send(new ProducerRecord<Integer, String>(data.getTopicName(), data.getMessage())).get();
				System.out.println("\n\nSent message: (Message : " + messageNo + ", " + "TOPIC : " + data.getTopicName()
						+ " Message :  " + data.getMessage() + ")\n\n");
			} catch (InterruptedException | ExecutionException e) {

				System.out.printf("\nExecption occurred while sending message %s", e.getMessage());
				return;

			}

		}
	}

	public void sendMessage(String topic, String message) {
		System.out.printf("\nSending message : %s to topic : %s", message,topic);
		long startTime = System.currentTimeMillis();
		if (isAsync) {
			producer.send(new ProducerRecord<Integer, String>(topic, message), new MessageCallBack(startTime, message));
		} else {
			try {
				producer.send(new ProducerRecord<Integer, String>(topic, message)).get();
				System.out.println("Message sent to topic : " + topic );
			} catch (InterruptedException | ExecutionException e) {

				System.out.printf("Execption occurred while sending message %s", e.getMessage());
				return;

			}

		}
	}
}

class MessageCallBack implements Callback {
	private final long startTime;
	private final String message;

	public MessageCallBack(long startTime, String message) {
		this.startTime = startTime;
		this.message = message;
	}

	
	public void onCompletion(RecordMetadata metadata, Exception exception) {
		long elapsedTime = System.currentTimeMillis() - startTime;
		if (metadata != null) {
			System.out.println("\n\nmessage(" + message + ") sent to partition(" + metadata.partition() + "), "
					+ "offset(" + metadata.offset() + ") in " + elapsedTime + " ms");
		} else {
			exception.printStackTrace();
		}
	}
}
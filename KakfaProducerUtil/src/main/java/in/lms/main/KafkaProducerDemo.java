package in.lms.main;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class KafkaProducerDemo {
	public static String INPUT_FILE = "";
	public static boolean isAsync = false;

	public static Map<Long, Data> dataMap = new HashMap<>();

	public static void main(String[] args) {

		System.out.println("Enter file path");
		Scanner scanner = new Scanner(System.in);

		INPUT_FILE = scanner.next();
		if (INPUT_FILE == null || INPUT_FILE.isEmpty()) {
			System.out.println("Invalid Path");
		}

		System.out.printf("\n\nEntered file path : %s\n\n", INPUT_FILE);

		MessageProducer producer = new MessageProducer("localhost:9092");
		producer.setIsAsync(isAsync);

		dataMap = DataFactory.getInstance().readData(INPUT_FILE);
		
		while (true) {
			try {

				performAction(scanner, producer);
			} catch (Exception e) {
				System.out.printf("\n\n Invalid input : %s \n\n", e.getMessage());
				scanner = new Scanner(System.in);
				performAction(scanner, producer);
			}
		}

	}

	private static void performAction(Scanner scanner, MessageProducer producer) {
		System.out.printf(
				"\n\nEnter message Number (range from %d to %d) as configured in csv or to load CSV enter %d\n\n", 1,
				dataMap.size(), 0);

		long input = scanner.nextLong();

		if (input==0L) {
			dataMap = DataFactory.getInstance().readData(INPUT_FILE);
			System.out.println("\n\nCSV reloaded !");
		} else {
			if (dataMap.get(input) == null) {
				System.out.printf("\n\nInvalid message number %d. Doesn't exist in csv \n\n", input);
				return;

			}
			producer.sendMessage(dataMap.get(input));
		}

	}
}